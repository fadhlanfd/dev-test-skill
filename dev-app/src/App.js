import React from 'react';
import RoutesData from './routes/Routes';

const App = () => {
  return (
    <div className="App">
      <RoutesData />
    </div>
  );
};

export default App;
