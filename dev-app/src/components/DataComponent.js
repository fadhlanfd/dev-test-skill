import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { fetchDataFromAPI } from '../redux/actions/dataAction';
import { Card, Input, Modal } from 'antd';
import ReactPlayer from 'react-player';
import './DataComponent.css';
import ApexLogo from '../assets/apex-logo.png';

const { Meta } = Card;

const DataComponent = ({ data, loading, error, fetchDataFromAPI }) => {
  const [filteredData, setFilteredData] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null); 
  const [uniqueCategories, setUniqueCategories] = useState([]);

  useEffect(() => {
    fetchDataFromAPI();
  }, [fetchDataFromAPI]);

  useEffect(() => {
    const filtered = data.filter(item =>
      item.displayName.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredData(filtered);
  }, [data, searchTerm]);

  useEffect(() => {
    const uniqueCategoriesSet = new Set(data.map(item => item.role));
    setUniqueCategories(Array.from(uniqueCategoriesSet)); 
  }, [data]);

  const handleSearch = e => {
    setSearchTerm(e.target.value);
  };

  const handleCardClick = item => {
    setSelectedItem(item);
    setModalVisible(true);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  const handleCategoryClick = category => {
    setSelectedCategory(category); 
    if (category === 'All') {
      setFilteredData(data);
    } else {
      const filteredByCategory = data.filter(item => item.role === category);
      setFilteredData(filteredByCategory);
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      <div className="data-color-wrapper">
      <div className="logo-container">
        <img src={ApexLogo} alt="Apex Legends Logo" className="apex-logo" />
      </div>
      
      <div className="home-category">
        <p>Home Category: {filteredData.length}</p>
        <div className="category-list">
          <div
            className={`category-item ${selectedCategory === 'All' ? 'active' : ''}`}
            onClick={() => handleCategoryClick('All')}
            style={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            ALL
          </div>
          {uniqueCategories.map(category => (
            <div
            key={category}
            className={`category-item ${selectedCategory === category ? 'active' : ''}`}
            onClick={() => handleCategoryClick(category)}
            style={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            {category}
          </div>          
          ))}
        </div>
      </div>
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '16px' }}>
    <Input
      placeholder="SEARCH"
      value={searchTerm}
      onChange={handleSearch}
      style={{
        maxWidth: '300px',
        backgroundColor: 'black',
        color: 'white', 
        border: '1.5px solid #f83337',
      }}
    />
    </div>
      <div className="data-container">
        {filteredData.map(item => (
          <div key={item.displayName} onClick={() => handleCardClick(item)}>
            <Card
              hoverable
              className="data-card"
              cover={<img alt={item.displayName} src={item.displayIcon} />}
            >
              <Meta title={item.displayName} />
            </Card>
          </div>
        ))}
      </div>
      <Modal
    title={selectedItem && selectedItem.displayName}
    visible={modalVisible}
    onCancel={handleModalClose}
    footer={null}
    centered 
    maskStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
    closable={false} 
    bodyStyle={{ padding: '20px', backgroundColor: '#1f1f1f' }}
    wrapClassName="custom-modal"
  >
  {selectedItem && (
    <div>
      <p style={{ color: '#fff', marginBottom: '16px' }}>
        Description: {selectedItem.description}
      </p>
      <p style={{ color: '#fff', marginBottom: '16px' }}>
        Role: {selectedItem.role}
      </p>
      <p style={{ color: '#fff', marginBottom: '16px' }}>Abilities:</p>
      <ul style={{ color: '#fff', marginBottom: '16px' }}>
        {selectedItem.abilities.map((ability, index) => (
          <li key={index}>
            {ability.displayName}: {ability.description}
          </li>
        ))}
      </ul>
      {selectedItem.video && (
        <div className="video-container">
          <ReactPlayer url={selectedItem.video} controls />
        </div>
      )}
    </div>
    )}
    <button className="close-button" onClick={handleModalClose}>
      Close
    </button>
  </Modal>
    </div>
  );
};

const mapStateToProps = state => ({
  data: state.data.data,
  loading: state.data.loading,
  error: state.data.error,
});

const mapDispatchToProps = {
  fetchDataFromAPI,
};

export default connect(mapStateToProps, mapDispatchToProps)(DataComponent);
