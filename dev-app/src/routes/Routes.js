import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import DataComponent from '../components/DataComponent';

const RoutesData = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<DataComponent />} />
      </Routes>
    </Router>
  );
};

export default RoutesData;
