import axios from 'axios';

const BASE_URL = 'https://staging.ina17.com';

const apiService = axios.create({
  baseURL: BASE_URL,
});

export const fetchData = async () => {
  try {
    const response = await apiService.get('/data.json');
    return response.data;
  } catch (error) {
    throw error;
  }
};